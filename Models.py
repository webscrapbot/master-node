import json
import random
from _ctypes_test import func
from datetime import datetime
from typing import List

import requests
from sqlalchemy import INTEGER, Column, Text, String, Boolean, ForeignKey, Table, Integer, Sequence, TIMESTAMP, \
    SMALLINT, UniqueConstraint, and_
from sqlalchemy.exc import IntegrityError
from sqlalchemy import func
from sqlalchemy.orm import Session, noload
from sqlalchemy.orm import relationship

from Factories import Base

# TODO: See bether way to avoid
jsoned_pages = {}


class Process(Base):
    __tablename__ = 'process'

    id = Column(INTEGER, Sequence('process_sequence'), primary_key=True)
    ip_initiator = Column(String(15), nullable=False)
    start_date = Column(TIMESTAMP)
    finish_date = Column(TIMESTAMP)

    page_processes: List['PageProcess'] = relationship("PageProcess")

    def start(self, db: 'Session'):
        self.start_date = datetime.now()

        db.add(self)
        db.flush()
        db.commit()

    def finish(self, db: 'Session'):
        self.finish_date = datetime.now()

        db.add(self)
        db.flush()
        db.commit()

    def to_json(self):
        return {
            'id': self.id,
            'ip_initiator': self.ip_initiator,
            'start_date': self.start_date,
            'finish_date': self.finish_date
        }


class Site(Base):
    __tablename__ = 'site'

    id = Column(INTEGER, Sequence('site_sequence'), primary_key=True)
    domain = Column(String(255), nullable=False)
    secure = Column(Boolean, default=True)

    pages: List['Page'] = relationship("Page")
    scrap_patterns: List['ScrapPattern'] = relationship("ScrapPattern", back_populates="site")

    def to_json(self):
        data = {'id': self.id, 'domain': self.domain, 'secure': self.secure}

        pages = []

        for page in self.pages:
            pages.append(page.to_json())

        data['pages'] = pages

        scrap_patterns = []

        for scrap_pattern in self.scrap_patterns:
            scrap_patterns.append(scrap_pattern.to_json())

        data['scrap_patterns'] = scrap_patterns

        return data

    def _append_page(self, page: 'Page'):
        page_appended = False

        for appended_page in self.pages:
            if appended_page.path == page.path:
                page_appended = True
                break

        if not page_appended:
            self.pages.append(page)

    def _append_scrap_pattern(self, scrap_pattern: 'ScrapPattern'):
        scrap_pattern_appended = False

        for appended_scrap_pattern in self.scrap_patterns:
            if appended_scrap_pattern.id == scrap_pattern.id:
                scrap_pattern_appended = True
                break

        if not scrap_pattern_appended:
            self.scrap_patterns.append(scrap_pattern)

    @staticmethod
    def normalize_instanciate(data: dict, db: 'Session', process: 'Process' = None,
                              slave_node: 'SlaveNode' = None) -> 'Site':

        if 'id' in data:
            instance: 'Site' = db.query(Site) \
                .options(noload("pages"), noload("scrap_patterns")) \
                .get(data['id'])
        elif 'domain' in data:
            instance: 'Site' = db.query(Site) \
                .options(noload("pages"), noload("scrap_patterns")) \
                .filter(Site.domain == data['domain']).first()

            if instance is None:
                instance = Site(domain=data['domain'])

                if 'secure' in data:
                    instance.secure = data['secure']

                db.add(instance)
                db.flush()
                db.commit()
        else:
            # TODO: Raise error
            raise Exception("Site domain or id not present")

        if 'pages' in data:
            if type(data['pages']) == list:
                for page in data['pages']:
                    instance._append_page(Page.normalize_instanciate(data=page, site=instance, db=db, process=process,
                                                                     slave_node=slave_node))
            else:
                # TODO: Lanunch error
                raise Exception("Pages type error")

        if 'scrap_patterns' in data:
            for scrap_pattern in data['scrap_patterns']:
                instance._append_scrap_pattern(
                    ScrapPattern.normalize_instanciate(data=scrap_pattern, site=instance, db=db))

        if 'scraped_infos' in data:
            print("--------------------------", data['scraped_infos'])

        return instance


class SlaveNode(Base):
    __tablename__ = 'slave_node'

    id = Column(INTEGER, Sequence('slave_node_sequence'), primary_key=True, unique=True)
    ip = Column(String(15), nullable=False)
    port = Column(SMALLINT, nullable=False)
    max_pages_scrapping = Column(INTEGER, default=5)

    __table_args__ = (UniqueConstraint('ip', 'port', name='unique_slave_node'),)
    page_processes: List['PageProcess'] = relationship("PageProcess")

    def to_json(self):
        return {
            'id': self.id,
            'ip': self.ip,
            'port': self.port,
            'max_pages_scrapping': self.max_pages_scrapping
        }

    @staticmethod
    def get_a_workable_slave_node(database: "Session") -> "SlaveNode":
        slave_node_id_pages_processing = database.query(
            PageProcess.slave_node_id,
            func.count(PageProcess.slave_node_id)
        ) \
            .join(Process) \
            .join(SlaveNode) \
            .group_by(PageProcess.slave_node_id) \
            .having(
            and_(
                Process.finish_date == None,
                func.count(PageProcess.slave_node_id) <= SlaveNode.max_pages_scrapping
            )
        ).all()

        if len(slave_node_id_pages_processing) > 0:
            return database \
                .query(SlaveNode) \
                .get(random.choice(slave_node_id_pages_processing)[0])
        else:
            query = database.query(SlaveNode)
            return query.offset(int(int(query.count()) * random.random())).first()

    def get_status(self) -> "SlaveNodeStatus":
        """
        Check for slave node status
        :return: SlaveNodeStatus status of slave node
        """

        try:
            r = requests.get(
                "http://" + self.ip + ":" + str(self.port) + "/status",
                timeout=10
            )

            return SlaveNodeStatus(
                status=str(r.status_code),
                slave_node=self
            )

        except OSError as oSError:
            if "Errno 113" in "{0}".format(oSError):
                return SlaveNodeStatus(
                    status=str("NO ROUTE TO HOST"),
                    slave_node=self
                )

            elif "timed out" in "{0}".format(oSError):
                return SlaveNodeStatus(
                    status=str("BUSY"),
                    slave_node=self
                )

    def process(self, site: dict, process: 'Process', db: 'Session') -> "Site":
        global jsoned_pages
        try:
            r = requests.post(
                "http://" + self.ip + ":" + str(self.port) + "/scrap",
                json.dumps({'data': site}),
                timeout=60,
                headers={'content-type': 'application/json'}
            )

            jsoned_pages = {}

            process.finish(db)

            return Site.normalize_instanciate(r.json()['data'], db=db, process=process, slave_node=self)
        except OSError as oSError:
            if "Errno 113" in "{0}".format(oSError):
                raise Exception("NO ROUTE TO HOST")
            elif "timed out" in "{0}".format(oSError):
                raise Exception("BUSY")


class Page(Base):
    __tablename__ = 'page'

    id = Column(INTEGER, Sequence('page_sequence'), primary_key=True)
    path = Column(Text, nullable=False)
    site_id = Column(ForeignKey('site.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    site: 'Site' = relationship('Site')
    page_bs: List['Page'] = relationship(
        'Page',
        secondary='page_relationship',
        primaryjoin='Page.id == page_relationship.c.page_a_id',
        secondaryjoin='Page.id == page_relationship.c.page_b_id'
    )
    processes: List['PageProcess'] = relationship("PageProcess")

    def to_json(self):
        global jsoned_pages

        if self.path not in jsoned_pages:
            crawled_pages = []
            processes = []

            returned_data = {
                'id': self.id,
                'path': self.path,
            }

            jsoned_pages[self.path] = returned_data

            for crawled_page in self.page_bs:
                crawled_pages.append(crawled_page.to_json())

            for process in self.processes:
                processes.append(process.to_json())

            returned_data['crawled_pages'] = crawled_pages
            returned_data['processed_infos'] = processes
        else:
            returned_data = jsoned_pages[self.path]

        return returned_data

    @staticmethod
    def normalize_instanciate(data: dict, site: 'Site', db: Session, process: 'Process' = None,
                              slave_node: 'SlaveNode' = None) -> 'Page':
        if 'id' in data:
            instance = db.query(Page) \
                .options(noload("page_bs"), noload("processes")) \
                .get(data['id'])
        elif 'path' in data:
            instance = db.query(Page).filter(Page.path == data['path'], Site.id == site.id) \
                .options(noload("page_bs"), noload("processes")) \
                .first()

            if instance is None:
                instance = Page()
                instance.path = data['path']
                instance.site = site
                instance.page_bs = []
                instance.processes = []

                db.add(instance)
                db.flush()
        else:
            raise Exception("ID or path not in data")

        if 'path_crawled_pages' in data:
            if type(data['path_crawled_pages']) == list:
                for path_crawled in data['path_crawled_pages']:
                    page_b = Page.normalize_instanciate({'path': path_crawled}, site=site, db=db)

                    instance.page_bs.append(page_b)
            else:
                # TODO: Lanunch error
                raise Exception("Path_crawled_pages type error")

        if 'scrapped_infos' in data:
            if type(data['scrapped_infos']) == list:
                if process is not None and slave_node is not None:
                    page_process = PageProcess.normalize_instanciate(data={}, db=db, process=process,
                                                                     page=instance, slave_node=slave_node)

                    db.add(page_process)
                    db.flush()

                    instance.processes.append(page_process)

                    for container_infos in data['scrapped_infos']:
                        result_container = ResultContainer.normalize_instanciate(
                            data={},
                            page_process=page_process,
                            container_scrap_pattern=ContainerScrapPattern.normalize_instanciate(
                                data={'id': container_infos['id']},
                                db=db
                            ),
                            db=db
                        )

                        if type(container_infos['scrapped_info_datas']) == list:
                            for scrapped_info in container_infos['scrapped_info_datas']:
                                if 'info_pattern_id' in scrapped_info:
                                    if scrapped_info['info'] is not None:
                                        result = Result.normalize_instanciate(
                                            data={"result": scrapped_info['info']},
                                            result_container=result_container,
                                            info_scrap_pattern=InfoScrapPattern.normalize_instanciate(
                                                data={'id': scrapped_info['info_pattern_id']}, db=db),
                                            db=db
                                        )
                                else:
                                    raise Exception("Info_pattern_id is null")
                        else:
                            # TODO: Lanunch error
                            raise Exception("Scrapped info datas type error")
                else:
                    raise Exception("Process or slave_node or info_pattern_id is null")
            else:
                # TODO: Lanunch error
                raise Exception("ScrappedInfo type error")

        return instance


class ScrapPattern(Base):
    __tablename__ = 'scrap_pattern'

    id = Column(INTEGER, Sequence('scrap_pattern_sequence'), primary_key=True)
    site_id = Column(ForeignKey('site.id'), nullable=False, index=True)

    site: 'Site' = relationship('Site', back_populates="scrap_patterns")
    container: 'ContainerScrapPattern' = relationship('ContainerScrapPattern', uselist=False,
                                                      back_populates="scrap_pattern")
    infos_scrap_pattern: List['InfoScrapPattern'] = relationship('InfoScrapPattern')

    def to_json(self):
        data = {'id': self.id, 'container': self.container.to_json()}

        infos_scrap_pattern = []

        for info_scrap_pattern in self.infos_scrap_pattern:
            infos_scrap_pattern.append(info_scrap_pattern.to_json())

        data['infos_scrap_pattern'] = infos_scrap_pattern

        return data

    @staticmethod
    def normalize_instanciate(data: dict, site: 'Site', db: Session) -> 'ScrapPattern':
        if 'id' in data:
            instance = db.query(ScrapPattern).get(data['id'])
        elif 'container_scrap_pattern' in data:
            if 'pattern' in data['container_scrap_pattern']:
                instance = db.query(ScrapPattern) \
                    .join(ContainerScrapPattern, Site) \
                    .filter(
                    ContainerScrapPattern.pattern == data['container_scrap_pattern']['pattern'],
                    Site.id == site.id
                ) \
                    .first()

                if instance is None:
                    instance = ScrapPattern(site=site)

                    db.add(instance)
                    db.flush()
                    db.commit()
            else:
                # TODO: Raise error
                raise Exception("Faltando pattern do container_scrap_pattern")
        else:
            # TODO: Raise error
            raise Exception("Faltando container_scrap_pattern")

        instance.container = ContainerScrapPattern.normalize_instanciate(
            data=data['container_scrap_pattern'],
            scrap_pattern=instance,
            db=db
        )

        if 'info_scrap_patterns' in data:
            for info_scrap_pattern in data['info_scrap_patterns']:
                InfoScrapPattern.normalize_instanciate(data=info_scrap_pattern, scrap_pattern=instance, db=db)

        return instance


class SlaveNodeStatus(Base):
    __tablename__ = 'slave_node_status'

    id = Column(INTEGER, Sequence('slave_node_status_sequence'), primary_key=True)
    status = Column(String(45), nullable=False)
    connection_try_date = Column(TIMESTAMP, default=datetime.now())
    slave_node_id = Column(ForeignKey('slave_node.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,
                           index=True)

    slave_node = relationship('SlaveNode')


class ContainerScrapPattern(Base):
    __tablename__ = 'container_scrap_pattern'

    id = Column(INTEGER, Sequence('container_scrap_pattern_sequence'), primary_key=True)
    pattern = Column(Text, nullable=False)
    scrap_pattern_id = Column(ForeignKey('scrap_pattern.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,
                              index=True)

    scrap_pattern: 'ScrapPattern' = relationship('ScrapPattern', back_populates="container")

    result_containers: List['ResultContainer'] = relationship('ResultContainer')

    def to_json(self):
        return {
            'id': self.id,
            'pattern': self.pattern
        }

    @staticmethod
    def normalize_instanciate(data: dict, db: 'Session',
                              scrap_pattern: 'ScrapPattern' = None) -> 'ContainerScrapPattern':
        if 'id' in data:
            instance = db.query(ContainerScrapPattern).get(data['id'])
        elif 'pattern' in data and 'pattern' in data:
            if scrap_pattern is not None:
                instance = db.query(ContainerScrapPattern) \
                    .join(ScrapPattern) \
                    .filter(
                    ScrapPattern.id == scrap_pattern.id,
                    ContainerScrapPattern.pattern == data['pattern']
                ).first()

                if instance is None:
                    instance = ContainerScrapPattern(pattern=data['pattern'], scrap_pattern=scrap_pattern)

                    db.add(instance)
                    db.flush()
                    db.commit()
            else:
                # TODO: Raise error
                raise Exception("Scrap pattern are not set")
        else:
            # TODO: Raise error
            raise Exception("Pattern or id are not set")

        return instance


class InfoScrapPattern(Base):
    __tablename__ = 'info_scrap_pattern'

    id = Column(Integer, Sequence('info_scrap_pattern_sequence'), primary_key=True)
    title = Column(String(255), nullable=False)
    pattern = Column(Text, nullable=False)
    required_attribute = Column(String(255), nullable=False)
    results_limit = Column(INTEGER, default=0)
    scrap_pattern_id = Column(ForeignKey('scrap_pattern.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,
                              index=True)

    scrap_pattern: 'ScrapPattern' = relationship('ScrapPattern', back_populates="infos_scrap_pattern")
    results: List['Result'] = relationship('Result')

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title,
            'pattern': self.pattern,
            'required_attribute': self.required_attribute,
            'results_limit': self.results_limit
        }

    @staticmethod
    def normalize_instanciate(data: dict, db: Session, scrap_pattern: 'ScrapPattern' = None) -> 'InfoScrapPattern':
        if 'id' in data:
            instance = db.query(InfoScrapPattern) \
                .options(noload("results")) \
                .get(data['id'])
        elif 'title' in data and 'pattern' in data and 'required_attribute' in data and 'results_limit' in data:
            instance = db.query(InfoScrapPattern) \
                .join(ScrapPattern) \
                .options(noload("results")) \
                .filter(
                ScrapPattern.id == scrap_pattern.id,
                InfoScrapPattern.pattern == data['pattern']
            ) \
                .first()

            if instance is None:
                instance = InfoScrapPattern(
                    title=data['title'],
                    pattern=data['pattern'],
                    required_attribute=data['required_attribute'],
                    results_limit=data['results_limit'],
                    scrap_pattern=scrap_pattern
                )

                db.add(instance)
                db.flush()
                db.commit()
        else:
            raise Exception("Faltando pattern, title, required_attribute ou id")

        return instance


class PageProcess(Base):
    __tablename__ = 'page_process'

    id = Column(INTEGER, Sequence('page_process_sequence'), primary_key=True)
    process_id = Column(ForeignKey('process.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    page_id = Column(ForeignKey('page.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    slave_node_id = Column(ForeignKey('slave_node.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,
                           index=True)

    page: 'Page' = relationship('Page', back_populates="processes")
    process: 'Process' = relationship('Process', back_populates="page_processes")
    slave_node: 'SlaveNode' = relationship('SlaveNode', back_populates="page_processes")
    result_containers: List['ResultContainer'] = relationship('ResultContainer')

    def append_result_container(self, result_container: 'ResultContainer'):
        for appended_result_container in self.result_containers:
            if appended_result_container.id != result_container.id:
                self.result_containers.append(result_container)

    def to_json(self):
        return {
            'id': self.id,
            'start_date': self.process.start_date.timestamp(),
            'finish_date': self.process.finish_date.timestamp(),
            'slave_node': self.slave_node.to_json(),
        }

    @staticmethod
    def normalize_instanciate(data: dict, db: 'Session', page: 'Page' = None, process: 'Process' = None,
                              slave_node: 'SlaveNode' = None) -> 'PageProcess':

        if 'id' in data:
            instance = db.query(PageProcess).get(data['id'])
        else:
            if page and process and slave_node:
                instance = PageProcess(page=page, process=process, slave_node=slave_node)

                if 'result_containers' in data:
                    if type(data['result_containers']) == list:
                        for result_container in data['result_containers']:
                            if isinstance(result_container, ResultContainer):
                                instance.append_result_container(result_container)
                            else:
                                raise Exception("Result container type is invalid")

                db.add(instance)
                db.flush()
                db.commit()
            else:
                raise Exception("Page, process or slave node are not set")

        return instance


class PageRelationship(Base):
    __tablename__ = 'page_relationship'

    id = Column(INTEGER, Sequence('page_relationship_sequence'), primary_key=True)
    page_a_id = Column(ForeignKey('page.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    page_b_id = Column(ForeignKey('page.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)

    page_a = relationship('Page', primaryjoin='PageRelationship.page_a_id == Page.id')
    page_b = relationship('Page', primaryjoin='PageRelationship.page_b_id == Page.id')

    @staticmethod
    def normalize_instanciate(data: dict, site: 'Site', db: Session = None) -> 'PageRelationship':
        if 'page_a' in data and 'page_b' in data:
            if data['page_a'].id and data["page_b"].id:
                instance = db.query(PageRelationship).filter(
                    PageRelationship.page_a_id == data['page_a'].id,
                    PageRelationship.page_b_id == data['page_b'].id
                ).first()

                if not instance:
                    instance = PageRelationship(page_a=data['page_a'], page_b=data['page_b'])

                    db.add(instance)
                    db.flush()
                    db.commit()

                return instance
        else:
            raise Exception("Page a or b not are in data")


class ResultContainer(Base):
    __tablename__ = 'result_container'

    id = Column(INTEGER, Sequence('result_container_sequence'), primary_key=True)

    page_process_id = Column(ForeignKey('page_process.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,
                             index=True)
    container_scrap_pattern_id = Column(
        ForeignKey('container_scrap_pattern.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False,
        index=True)

    page_process: 'PageProcess' = relationship('PageProcess', back_populates="result_containers")

    results: List['Result'] = relationship('Result')
    container_scrap_pattern: 'ContainerScrapPattern' = relationship('ContainerScrapPattern',
                                                                    back_populates="result_containers")

    @staticmethod
    def normalize_instanciate(data: dict, page_process: 'PageProcess', container_scrap_pattern: 'ContainerScrapPattern',
                              db: Session = None) -> 'ResultContainer':
        if 'id' in data:
            instance = db.query(ResultContainer).get(data['id'])
        else:
            instance = ResultContainer(page_process=page_process, container_scrap_pattern=container_scrap_pattern)

            db.add(instance)
            db.flush()
            db.commit()

        return instance


class Result(Base):
    __tablename__ = 'result'

    id = Column(INTEGER, Sequence('result_sequence'), primary_key=True)
    result = Column(Text, nullable=False)
    result_container_id = Column(ForeignKey('result_container.id', ondelete='CASCADE', onupdate='CASCADE'),
                                 nullable=False,
                                 index=True)
    info_scrap_pattern_id = Column(ForeignKey('info_scrap_pattern.id', ondelete='CASCADE', onupdate='CASCADE'),
                                   nullable=False,
                                   index=True)

    info_scrap_pattern: 'InfoScrapPattern' = relationship('InfoScrapPattern', back_populates="results")
    result_container: 'ResultContainer' = relationship('ResultContainer', back_populates="results")

    @staticmethod
    def normalize_instanciate(data: dict, info_scrap_pattern: 'InfoScrapPattern', result_container: 'ResultContainer',
                              db: Session) -> 'Result':
        if 'result' in data:
            instance = Result(result=data['result'], info_scrap_pattern=info_scrap_pattern,
                              result_container=result_container)

            db.add(instance)
            db.flush()
            db.commit()

            return instance
        else:
            raise Exception("Result not are in data")


class HttpSuccessResponseData:
    objects: List[dict]

    def __init__(self, objects=None):
        if objects is None:
            objects = []
        self.objects = objects


class HttpSuccessResponse:
    code: str
    data: "HttpSuccessResponseData"

    def __init__(self, code: str = "200", data: "HttpSuccessResponseData" = HttpSuccessResponseData()):
        self.code = code
        self.data = data


class HttpFailResponseData:
    statement: str
    message: str

    def __init__(self, statement: str, message: str):
        self.statement = statement
        self.message = message


class HttpFailResponse:
    code: str = None
    data: "HttpFailResponseData" = None

    def __init__(self, code: str, data: "HttpFailResponseData"):
        self.code = code
        self.data = data

    @staticmethod
    def get_instance(error: "IntegrityError"):
        return HttpFailResponse(
            code=error.code,
            data=HttpFailResponseData(
                statement=error.statement,
                message=error.orig
            )
        )
