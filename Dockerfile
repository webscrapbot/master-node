FROM python:3.6.9-buster

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV MAX_THREADS 5
ENV DEFAULT_TIMEOUT 60
ENV MASTER_NODES=""
ENV APP_PORT=8080
ENV MAX_PAGES_SCRAPPING=5
# Python, don't write bytecode!
ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get update

RUN apt-get install -y pipenv nano cron

VOLUME /app /data

WORKDIR /app

CMD pipenv install && pipenv run python __init__.py