from typing import List

from marshmallow_sqlalchemy import ModelSchema

import Models
from marshmallow import Schema, fields


class HttpFailResponseDataSchema(Schema):
    statement: str = fields.Str()
    message: str = fields.Str()


class HttpFailResponseSchema(Schema):
    code = fields.Str()
    data = fields.Nested(HttpFailResponseDataSchema())


class HttpSuccessResponseDataSchema(Schema):
    objects = fields.List(fields.Dict())


class HttpSuccessResponseSchema(Schema):
    code = fields.Str()
    data = fields.Nested(HttpSuccessResponseDataSchema())


class ProcessSchema(ModelSchema):
    class Meta:
        model = Models.Process


class PageProcessSchema(ModelSchema):
    class Meta:
        model = Models.PageProcess


class SlaveNodeSchema(ModelSchema):
    class Meta:
        model = Models.SlaveNode


class SlaveNodeStatusSchema(ModelSchema):
    class Meta:
        model = Models.SlaveNodeStatus


class SiteSchema(ModelSchema):
    class Meta:
        model = Models.Site


class PageSchema(ModelSchema):
    class Meta:
        model = Models.Page


class ScrapPatternSchema(ModelSchema):
    class Meta:
        model = Models.ScrapPattern


class ContainerScrapPatternSchema(ModelSchema):
    class Meta:
        model = Models.ContainerScrapPattern


class InfoScrapPatternSchema(ModelSchema):
    class Meta:
        model = Models.InfoScrapPattern
