# !/usr/bin/python
import json
from builtins import print

import bottle
from bottle import route, hook, response, request, abort, HTTPResponse
from bottle_sqlalchemy import SQLAlchemyPlugin
from crontab import CronTab, CronItem

import Schemas
from Enums import DatabaseConnectionType
from Factories import DatabaseFactory, Base
from sqlalchemy.orm import sessionmaker

import Models  # Just for create all models
from Routes import define_routes


@hook('before_request')
def check_content_type():
    if request.method == "POST":
        if request.headers.environ['CONTENT_TYPE'] != "application/json":
            raise HTTPResponse(
                json.dumps(
                    dict(
                        code="wncnty",  # Wrong Content Type
                        data=dict(
                            statement="",
                            message="Wrong request's content type"
                        )
                    )
                ),
                code=400,
                status=400,
                headers={
                    "Content-Type": "application/json"
                }
            )


@hook('after_request')
def enable_cors_set_content_type():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
    response.headers['Content-Type'] = 'application/json'

define_routes()

# this_dir = os.path.dirname(os.path.abspath(__file__))
# with open("{}/swagger.yml".format(this_dir)) as f:
#     swagger_def = yaml.load(f, Loader=yaml.FullLoader)

# install(SwaggerPlugin(swagger_def))

bottle.install(
    SQLAlchemyPlugin(
        DatabaseFactory.get_engine(DatabaseConnectionType.LOCAL),
        Base.metadata,
        create=True,
        commit=True,  # If it is true, plugin commit changes after route is executed (default True).
        create_session=sessionmaker(bind=DatabaseFactory.get_engine(DatabaseConnectionType.LOCAL)),
        keyword='local_db',  # Keyword used to inject session database in a route (default 'db').
    )
)

bottle.install(
    SQLAlchemyPlugin(
        DatabaseFactory.get_engine(DatabaseConnectionType.GLOBAL),
        Base.metadata,
        create=True,
        commit=True,  # If it is true, plugin commit changes after route is executed (default True).
        create_session=sessionmaker(bind=DatabaseFactory.get_engine(DatabaseConnectionType.GLOBAL)),
        keyword='global_db',  # Keyword used to inject session database in a route (default 'db').
    )
)

if __name__ == '__main__':
    system_cron = CronTab(user='root')

    curl_for_slave_nodes_status_exists = False

    for job in system_cron:
        job: CronItem = job

        if job.command == "curl localhost:8080/check_slaves_nodes":
            curl_for_slave_nodes_status_exists = True

    if not curl_for_slave_nodes_status_exists:
        job = system_cron.new(command='curl localhost:8080/check_slaves_nodes', user="root")
        job.minute.every(1)

        system_cron.write()

    bottle.debug(True)
    bottle.run(reloader=True, host='0.0.0.0', port=8080)
