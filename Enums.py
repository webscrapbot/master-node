from enum import Enum


class DatabaseConnectionType(Enum):
    LOCAL = 0,
    GLOBAL = 1