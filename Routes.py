import json
from typing import List

from bottle import route, request, response, HTTPResponse
from sqlalchemy import exc
from sqlalchemy.orm import Session

import Models
import Schemas


def define_routes():
    @route("/check_slaves_nodes", method="GET")
    def check_slave_nodes(local_db: 'Session'):
        slave_nodes: List["Models.SlaveNode"] = local_db.query(Models.SlaveNode).all()

        for slave_node in slave_nodes:
            local_db.add(slave_node.get_status())

        local_db.flush()

    @route("/register_slave_node", method="POST")
    def register_slave_node(local_db: 'Session'):
        try:
            if 'data' in request.json:
                if not request.json['data']['isASlaveNode']:
                    local_db.add_all(Schemas.SlaveNodeSchema().load(
                        request.json['data'],
                        session=local_db,
                        many=True
                    ).data)
                else:
                    data = request.json['data']

                    local_db.add(Schemas.SlaveNodeSchema().load({
                        'ip': request.environ.get('REMOTE_ADDR'),
                        'port': int(data['port']),
                        'max_pages_scraping': int(data['max_pages_scraping'])
                    }, session=local_db).data)

                local_db.flush()

                return Schemas.HttpSuccessResponseSchema().dumps(
                    Models.HttpSuccessResponse()
                ).data
            else:
                raise HTTPResponse(
                    json.dumps(
                        dict(
                            code="rewitdat",  # Request Without Data
                            data=dict(
                                statement="",
                                message="Request without data"
                            )
                        )
                    ),
                    code=400,
                    status=400,
                    headers={
                        "Content-Type": "application/json"
                    }
                )
        except exc.IntegrityError as e:
            local_db.rollback()

            response.status = 409

            return Schemas.HttpFailResponseSchema().dumps(
                Models.HttpFailResponse.get_instance(e)
            ).data

    @route("/start_process", method="POST")
    def start_process(local_db: 'Session'):
        if local_db.query(Models.SlaveNode).count() > 0:
            if 'data' in request.json:
                workable_slave_node: Models.SlaveNode = Models.SlaveNode.get_a_workable_slave_node(local_db)

                process: 'Models.Process' = Models.Process(
                    ip_initiator=request.environ.get('REMOTE_ADDR')
                )

                local_db.add(process)
                local_db.flush()

                process.start(db=local_db)

                returned_sites: List[dict] = []

                if type(request.json['data']) == list:
                    for site in request.json['data']:
                        # TODO: Count pages working and optimize send
                        returned_sites.append(
                            workable_slave_node.process(
                                Models.Site.normalize_instanciate(site, local_db).to_json(),
                                process=process,
                                db=local_db,
                            ).to_json()
                        )
                else:
                    raise HTTPResponse(
                        json.dumps(
                            dict(
                                code="dttynsup",  # Data Type Not Supported
                                data=dict(
                                    statement="",
                                    message="The data type is not supported"
                                )
                            )
                        ),
                        code=400,
                        status=400,
                        headers={
                            "Content-Type": "application/json"
                        }
                    )

                for origin_site in request.json['data']:
                    for returned_site in returned_sites:
                        new_returned_pages = []

                        for returned_page in returned_site['pages']:
                            returned_page_found = False

                            for origin_page in origin_site['pages']:
                                if origin_page['path'] == returned_page['path']:
                                    returned_page_found = True

                            if returned_page_found:
                                new_returned_pages.append(returned_page)

                        returned_site['pages'] = new_returned_pages

                return {'data': returned_sites}
            else:
                raise HTTPResponse(
                    json.dumps(
                        dict(
                            code="rewitdat",  # Request Without Data
                            data=dict(
                                statement="",
                                message="Request without data"
                            )
                        )
                    ),
                    code=400,
                    status=400,
                    headers={
                        "Content-Type": "application/json"
                    }
                )
        else:
            response.status = 424

            return Schemas.HttpFailResponseSchema().dumps(
                Models.HttpFailResponse(
                    code="empslvnd",
                    data=Models.HttpFailResponseData(
                        statement="",
                        message="Please register a slave node before work in sites pages"
                    )
                )
            ).data
