from typing import List

from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

from Enums import DatabaseConnectionType

Base = declarative_base()


class DatabaseFactory:
    @staticmethod
    def get_engine(connection_type: "DatabaseConnectionType") -> Engine:

        if connection_type == DatabaseConnectionType.LOCAL:
            engine = create_engine('sqlite:////data/local.db', echo=True)
        else:
            engine = create_engine('mysql+mysqldb://root:devinart2016@172.17.0.4:3306/webscrapbot', echo=True)

        return engine
